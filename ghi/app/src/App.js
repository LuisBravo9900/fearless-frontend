import React from "react";
import Nav from './Nav';
import LocationForm from './LocationForm';
import AttendeesList from "./AttendeesList";
import ConferenceForm from "./ConferenceForm"

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
      <Nav />
      <div className="container">
        {/* <LocationForm /> */}
        {/* <AttendeesList attendees={props.attendees} /> */}
        <ConferenceForm />
      </div>
    </>
  );
}

export default App;
